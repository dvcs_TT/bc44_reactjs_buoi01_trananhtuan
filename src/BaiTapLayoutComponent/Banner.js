import React, { Component } from 'react'

export default class Banner extends Component {
    render() {
        return (
            <div className="py-5">
                <div className="container px-lg-5">
                    <div className="p-4 p-lg-2 bg-light rounded-3 text-left">
                        <div className="m-1 m-lg-5">
                            <h1 className="display-6">A Warm Welcome!</h1>
                            <p className="fs-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit molestiae laboriosam quam voluptatum magnam. Aspernatur unde vitae labore odit. Eos sequi iusto officiis neque est. Impedit pariatur ipsam voluptatibus dignissimos!</p>
                            <a className="btn btn-primary btn-lg" href="#!">Call to action!</a>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
