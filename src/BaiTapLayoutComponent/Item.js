import React, { Component } from "react";

export default class Item extends Component {
  render() {
    return (
      <section className="pt-4">
        <div className="container px-lg-5">
          <div className="row gx-lg-5">
            <div className="col-lg-3 col-xxl-6 mb-5">
              <div className="card bg-light border-0 h-100">

                <div className="card-header text-center text-secondary bg-info d-flex justify-content-center align-items-center" style={{height: 150}}>
                    <p className="mb-0"> 500 x 325 </p>
                </div>

                <div className="card-body text-center p-0 p-lg-2 pt-0 pt-lg-0">
                  <h2 className="fs-4 fw-bold">Card title</h2>
                  <p className="mb-5 p-0">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Consequuntur cupiditate facere in.
                  </p>
                  <a class="btn btn-primary" href="#!">
                    Find Out More!
                  </a>
                </div>

              </div>
            </div>

            <div className="col-lg-3 col-xxl-6 mb-5">
              <div className="card bg-light border-0 h-100">

                <div className="card-header text-center text-secondary bg-info d-flex justify-content-center align-items-center" style={{height: 150}}>
                    <p className="mb-0"> 500 x 325 </p>
                </div>

                <div className="card-body text-center p-0 p-lg-2 pt-0 pt-lg-0">
                  <h2 className="fs-4 fw-bold">Card title</h2>
                  <p className="mb-5 p-0">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Consequuntur cupiditate facere in.
                  </p>
                  <a class="btn btn-primary" href="#!">
                    Find Out More!
                  </a>
                </div>

              </div>
            </div>

            <div className="col-lg-3 col-xxl-6 mb-5">
              <div className="card bg-light border-0 h-100">

                <div className="card-header text-center text-secondary bg-info d-flex justify-content-center align-items-center" style={{height: 150}}>
                    <p className="mb-0"> 500 x 325 </p>
                </div>

                <div className="card-body text-center p-0 p-lg-2 pt-0 pt-lg-0">
                  <h2 className="fs-4 fw-bold">Card title</h2>
                  <p className="mb-5 p-0">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Consequuntur cupiditate facere in.
                  </p>
                  <a class="btn btn-primary" href="#!">
                    Find Out More!
                  </a>
                </div>

              </div>
            </div>

            <div className="col-lg-3 col-xxl-6 mb-5">
              <div className="card bg-light border-0 h-100">

                <div className="card-header text-center text-secondary bg-info d-flex justify-content-center align-items-center" style={{height: 150}}>
                    <p className="mb-0"> 500 x 325 </p>
                </div>

                <div className="card-body text-center p-0 p-lg-2 pt-0 pt-lg-0">
                  <h2 className="fs-4 fw-bold">Card title</h2>
                  <p className="mb-5 p-0">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Consequuntur cupiditate facere in.
                  </p>
                  <a class="btn btn-primary" href="#!">
                    Find Out More!
                  </a>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
